rmdir /s /q built
docker build . -f builders/windows/Dockerfile -t slice3d-demo-physics-windows --build-arg DEBUG="-debug"
if %errorlevel% neq 0 exit /b %errorlevel%
docker create --name slice3d-demo-physics-windows slice3d-demo-physics-windows
docker cp slice3d-demo-physics-windows:/game/built built
docker rm slice3d-demo-physics-windows
cd built
slice3d-demo-physics.exe
