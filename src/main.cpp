#include <slice.h>
#include <slicefps.h>
#include <sliceopts.h>
#include <slice3d.h>

#include "charctrl.h"

s3dRenderCamera* camera;

void Draw ()
{
    s3dUpdateShadows();
    camera->Resize(slVec2(1));
	camera->Render();
	slDrawUI();
};

// To do: make mouse sensitivity adjustable in options menu.
slScalar HorizontalSensitivity = 0.05;
slScalar VerticalSensitivity = 0.05;
void OnInputEvent (SDL_Event event)
{
	if (event.type == SDL_MOUSEMOTION)
	{
        s3dVec3 ypr = camera->GetYPR();
        ypr.x += event.motion.xrel * HorizontalSensitivity;
		ypr.y -= event.motion.yrel * VerticalSensitivity;
		ypr.y = slfmin(90,slfmax(-90,ypr.y));
        camera->SetYPR(ypr);
	};
};

void OnOptsVisi (bool visi)
{
	// Purpose of this: if the user hits Escape and goes in the options menu, turn off relative mouse mode.
	if (visi) SDL_SetRelativeMouseMode(SDL_FALSE);
	else SDL_SetRelativeMouseMode(SDL_TRUE);
};

slList boxes("boxes",slNoIndex);
void ClearBoxes ()
{
	boxes.Clear(phys3dDestroyObject);
};
void ResetBoxes ()
{
	ClearBoxes(); // Remove all existing boxes first.



	s3dVec3 box_size = s3dVec3(0.5);
	s3dVec3 box_halfsize = box_size * 0.5;
	s3dModelRef box_model = s3dLoadObj("content/","testbox.s3dm.obj");//,s3dVec3(-.5));
	phys3dShape* box_shape = phys3dMakeShape(new btBoxShape(btVector3(box_halfsize.xyz)));
	for (slBU meta_x = 0; meta_x < 8; meta_x++) for (slBU meta_z = 0; meta_z < 8; meta_z++)
	{
		for (slBU z = 0; z < 2; z++)
		{
			for (slBU y = 0; y < 2; y++)
			{
				for (slBU x = 0; x < 2; x++)
				{
					s3dVec3 box_pos = s3dVec3(x*1.5+meta_x*5,y+.55,z*-1.5+meta_z*-5.) * box_size * 1.1;

					s3dObject* box = s3dCreateObject(box_model);
					phys3dObject* phys = phys3dCreateObject(box,box_shape,box_pos,1);
					phys->body->setFriction(0.8);
					//phys->body->setRestitution(1.5);
					phys->base_transform = GLmat4_scaling(GLvec4(box_size));

					boxes.Add(phys);
				};
			};
		};
	};
};

struct DebrisItem
{
	s3dObject* obj;
	slScalar ttl;
	slBU _index_;
};
slList Debris("Debris",offsetof(DebrisItem,_index_));
void AddDebris (s3dObject* obj, slScalar ttl)
{
	DebrisItem* item = new DebrisItem;
	item->obj = obj;
	item->ttl = ttl;
	Debris.Add(item);
};
void DestroyDebrisItem (DebrisItem* item)
{
	Debris.Remove(item);
	s3dDestroyObject(item->obj);
	delete item;
};
void StepDebrisItem (DebrisItem* item, void* userdata, slWorker* worker)
{
	item->ttl -= slGetDelta();
	//printf("%f\n",item->ttl);
	if (item->ttl <= 0) slSchedule(DestroyDebrisItem,item,worker);
};
void StepDebris ()
{
	slDoWork(&Debris,StepDebrisItem,NULL);
};
void QuitDebris ()
{
	Debris.UntilEmpty(DestroyDebrisItem);
};

struct ExplosionInfo
{
	s3dVec3 origin;
	slScalar radius,force;
};
void ExplosionTest (phys3dObject* box, ExplosionInfo* info)
{
	btTransform trans = box->body->getWorldTransform();
	btVector3 pos_bt = trans.getOrigin();
	s3dVec3 pos = s3dVec3::from_float_array(pos_bt.m_floats);
	s3dVec3 diff = pos - info->origin;
	slScalar dist = diff.length();
	if (dist < info->radius)
	{
		slScalar force_amount = info->force * (1 - (dist / info->radius));
		s3dVec3 force_vec = diff.normalized() * force_amount;
		box->body->applyCentralImpulse(btVector3(force_vec.xyz));
		box->body->activate(true); // Force the body to leave sleep mode, otherwise the impulse won't do anything.
	};
};
void Explosion (s3dVec3 origin, slScalar radius, slScalar force)
{
	s3dObject* boom = s3dCreateObject(s3dLoadObj("content/","explosion.s3dm.obj"));//,s3dVec3(-.5)));
	boom->SetWorldTransform(GLmat4_offset(GLvec4(origin)) * GLmat4_scaling(GLvec4(radius)));
	AddDebris(boom,0.1);
	ExplosionInfo info = {origin,radius,force};
	slDoWork(&boxes,ExplosionTest,&info);
};

extern phys3dObject* player;
btVector3 from_orientation (btScalar yaw, btScalar pitch)
{
	GLmat4 matrix = GLmat4_yrotate(slDegToRad_F(yaw)) * GLmat4_xrotate(slDegToRad_F(pitch));
	GLvec4 rotated = matrix * GLvec4(0,0,-1);
	return btVector3(rotated.x,rotated.y,rotated.z);
};
void Explode ()
{
	// Causes an explosion where the cursor is pointing.

	btVector3 start = btVector3(camera->GetOrigin().xyz);
	btDiscreteDynamicsWorld* world = phys3dGetWorld();
    s3dVec3 ypr = camera->GetYPR();
	btScalar len = 1000;
	btVector3 target = start + from_orientation(ypr.x,ypr.y) * len;
	btDiscreteDynamicsWorld::ClosestRayResultCallback closest(start,target);
	world->rayTest(start,target,closest);

	if (!closest.hasHit()) closest.m_hitPointWorld = target;
	//if (closest.hasHit())
	//{
		//printf("boom\n");
		Explosion(s3dVec3::from_float_array(closest.m_hitPointWorld.m_floats),5,20);
	//}
	//else printf("no hit\n");
};

int main (int argc, char** argv)
{
	/// Initialize engine and extensions.
	slInit();
	fpsInit();
	//fpsSetIndicatorEnabled(true);
	s3dInit();

    {
        s3dRenderCamera cam_obj;
        camera = &cam_obj;
        uiBoxRef cambox =
            (new uiBox)
            ->Attach()
            ->SetPos(0)
            ->SetSize(1)
            ->SetZ(255)
            ->SetTexture(cam_obj.GetColor())
        ;

        s3dLight* light = s3dLight::Create();
        light->color = GLcolor3(1,0.9,0.8);
        light->strength = 100;
        light->GetCamera()->SetOrigin(s3dVec3(0,50,0));
        light->GetCamera()->SetPitch(-90);
        light->SetCoverageAngle(90);
        light->SetShadowRes(2048);
        s3dLight* player_light = s3dLight::Create();
        player_light->color = GLcolor3(0.8,0.9,1);
        player_light->strength = 10;
        player_light->SetCoverageAngle(90);


    	/// Show logo sequence. Allow it to be skipped with a key or a command line argument.
    	if (GetArg(argc,argv,"-skip-logos")) goto SKIP_LOGOS;
    	slGetKeyBind("Skip Logo Sequence",slKeyCode(SDLK_ESCAPE))->onpress = OnSkipLogo;
    	DefaultLogoSequence(false,true);
    	SKIP_LOGOS:

    	opInit(); // *Now* initialize this. Didn't before logos, so that Escape didn't bring up the options menu while skipping logos.

    	/// Other initialization.
    	InitWalkingKeys(); // Set up the key binds for WASD+Jump.
    	slSetAppDrawStage(Draw); // Set the rendering procedure.
        camera->Perspective(90,0.1,1000); // To do: make FOV adjustable in options menu.

    	/// Mouse input for camera movement.
    	SDL_SetRelativeMouseMode(SDL_TRUE);
    	slSetAllEventsHandler(OnInputEvent);
    	opSetVisibilityCallback(OnOptsVisi);

    	/// Create player.
    	MakePlayer(1.75,0.5,s3dVec3(5,3,10));

    	// rudimentary cursor
    	slSetBoxDims(slCreateBox(slRenderText("+",{255,255,0,255})),.48,.48,.04,.04,255);

    	slGetKeyBind("Explode",slKeyCode(SDLK_e))->onpress = Explode;

    	/// Demo: Create the ground.
    	s3dModelRef groundmodel = s3dLoadObj("content/","ground.s3dm.obj");
    	s3dObject* ground = s3dCreateObject(groundmodel);
    	s3dVec3 ground_pos = s3dVec3(0,-1,0);
    	ground->SetWorldTransform(GLmat4_offset(GLvec4(ground_pos)));
    	phys3dShape* groundshape = phys3dMakeShape(new btBoxShape(btVector3(100,1,100)));
    	phys3dObject* plane = phys3dCreateObject(ground,groundshape,ground_pos,0);
    	plane->body->setFriction(1);

    	/// Demo: Spawn those dynamic cubes on the map.
    	//ResetBoxes();
    	slGetKeyBind("Reset Boxes",slKeyCode(SDLK_r))->onpress = ResetBoxes;

    	/// Demo: Create the door object.
    	s3dObject* doorvis = s3dCreateObject(s3dLoadObj("content/","door.s3dm.obj"));
    	phys3dShape* doorshape = phys3dMakeShape(new btBoxShape(btVector3(0.5,1,0.05)));
    	phys3dObject* door = phys3dCreateObject(doorvis,doorshape,s3dVec3(0,1,5),0);

    	/// Demo: Create the door object.
    	s3dObject* diagvis = s3dCreateObject(s3dLoadObj("content/","door.s3dm.obj"));
        phys3dShape* diagshape = phys3dMakeShape(new btBoxShape(btVector3(2,4,0.2)));
    	phys3dObject* diag = phys3dCreateObject(diagvis,diagshape,s3dVec3(-5,2,0),0);

        btTransform diag_transform = diag->body->getWorldTransform();
        diag_transform.setRotation(btQuaternion(slDegToRad(45),0,0));
        diag->body->setWorldTransform(diag_transform);
        diag->body->getMotionState()->setWorldTransform(diag_transform);
        diag->base_transform = GLmat4_scaling({4});

        for (int ang = 3; ang <= 45; ang += 3)
        {
            s3dObject* rampvis = s3dCreateObject(s3dLoadObj("content/","door.s3dm.obj"));
            phys3dShape* rampshape = phys3dMakeShape(new btBoxShape(btVector3(.5,8,0.2)));
            phys3dObject* ramp = phys3dCreateObject(rampvis,rampshape,s3dVec3(-5 - (ang/3),0,-5),0);

            btTransform ramp_transform = ramp->body->getWorldTransform();
            ramp_transform.setRotation(btQuaternion(0,slDegToRad(90 - ang),0));
            ramp->body->setWorldTransform(ramp_transform);
            ramp->body->getMotionState()->setWorldTransform(ramp_transform);
            ramp->base_transform = GLmat4_scaling({1,8,4});
        }

    	/// Main loop.
    	//slScalar elapsed = 0;
    	while (!slGetReqt())
    	{
    		/// Demo: Induce severe nausea.
    		//elapsed += slGetDelta();
    		//slScalar CamRoll = sin(elapsed * M_PI) * 10;
    		//s3dSetCamRotations(NULL,NULL,&CamRoll);

    		/// Update camera.
    		//FreeCamStep();
    		PlayerCamStep();

            player_light->GetCamera()->SetOrigin(camera->GetOrigin());
            player_light->GetCamera()->SetYPR(camera->GetYPR());

    		StepDebris();

    		/// Cycle the engine.
    		slCycle();
    	};

    	ClearBoxes();
    	QuitDebris();
        s3dLight::Destroy(light);
        s3dLight::Destroy(player_light);
    }

	/// Quit engine and extensions.
	s3dQuit();
	opQuit();
	fpsQuit();
	slQuit();
};
