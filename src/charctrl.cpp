#include <slice.h>
#include <slice3d.h>

slKeyBind* MoveForward;
slKeyBind* MoveBackward;
slKeyBind* MoveLeft;
slKeyBind* MoveRight;
//slKeyBind* MoveUp;
//slKeyBind* MoveDown;
slKeyBind* Jump;

extern s3dRenderCamera* camera;

slScalar FreeCamSpeed = 5;
void FreeCamStep ()
{
	s3dVec3 cam_inputs = s3dVec3(
		(MoveRight->down ? 1 : 0) - (MoveLeft->down ? 1 : 0),
		0,//(MoveUp->down ? 1 : 0) - (MoveDown->down ? 1 : 0),
		(MoveBackward->down ? 1 : 0) - (MoveForward->down ? 1 : 0)
	) * slGetDelta() * FreeCamSpeed;
    s3dVec3 ypr = camera->GetYPR();
	GLmat4 cam_input_rotation = GLmat4_yrotate(   slDegToRad_F(ypr.x))
                                                * GLmat4_xrotate(slDegToRad_F(ypr.y));
    camera->SetOrigin(camera->GetOrigin() + s3dVec3(cam_input_rotation * cam_inputs));
}

btVector3 LinearForceCurve (btVector3 current_velocity, btVector3 target_velocity, btScalar idle_force, btScalar unit, btVector3 mask = btVector3(1,1,1))
{
	btVector3 increase_amount = target_velocity - current_velocity;
	increase_amount *= mask;
	btScalar increase_length = increase_amount.length();
	if (increase_length > unit) increase_amount *= unit / increase_length;
	return (increase_amount / unit) * idle_force;
}



phys3dObject* player = NULL;
slScalar WalkSpeed = 5;
slScalar character_head_offset;
btScalar jump_ray_len;

bool OnGround ()
{
    if (!player) return false;

    // Ray cast downward from player and see if something is directly below.
    btVector3 start = player->GetBodyCenter();
    btVector3 target = start + btVector3(0,-jump_ray_len,0);

    btDiscreteDynamicsWorld* world = phys3dGetWorld();
	btDiscreteDynamicsWorld::ClosestRayResultCallback closest(start,target);
	world->rayTest(start,target,closest);
    return closest.hasHit();
}

void OnJump ();
slScalar jump_cooldown = -1;

void PlayerCamStep ()
{
	if (player)
	{
		btTransform player_transform;
		player->body_motion_state->getWorldTransform(player_transform);
        camera->SetOrigin(s3dVec3::from_float_array(player_transform.getOrigin().m_floats) + s3dVec3(0,character_head_offset,0));

        // Disallow direction changes mid-air.
        if (OnGround())
        {
    		slVec2 inputs = slVec2((MoveRight->down ? 1 : 0) - (MoveLeft->down ? 1 : 0),(MoveBackward->down ? 1 : 0) - (MoveForward->down ? 1 : 0));
    		inputs = slRotatePoint(inputs,-camera->GetYaw());

    		btVector3 vel_target = btVector3(inputs.x,0,inputs.y);
    		vel_target.safeNormDefZero();
    		vel_target *= WalkSpeed;
    		btVector3 force = LinearForceCurve(player->body->getLinearVelocity(),
                                               vel_target,
                                               500,
                                               WalkSpeed,
                                               btVector3(1,0,1));
    		player->body->applyCentralForce(force);

            jump_cooldown -= slGetDelta();
            if (Jump->down && jump_cooldown <= 0) OnJump();
        }
	}
}

void MakePlayer (btScalar total_height, btScalar radius, s3dVec3 start_pos)
{
	if (player) phys3dDestroyObject(player);

	btScalar inner_height = total_height - radius * 2;

	// human height: 1.75 meters
	phys3dShape* player_shape = phys3dMakeShape(new btCapsuleShape(radius,inner_height)); // height does NOT include the radii at the 'ends'.
	player = phys3dCreateObject(NULL,player_shape,start_pos,10);
	player->body->setAngularFactor(0);
	player->body->setSleepingThresholds(0,0);
	player->body->setFriction(0);
	//player->body->setFriction(1.5);

	character_head_offset = inner_height * 0.5;
    jump_ray_len = total_height * 0.5 + 0.1;
}

void OnJump ()
{
	if (player && OnGround())
	{
		player->body->applyCentralImpulse(btVector3(0,60,0));
        jump_cooldown = 0.1;
	}
}

void InitWalkingKeys ()
{
	MoveForward = slGetKeyBind("Move Forward",slKeyCode(SDLK_w));
	MoveBackward = slGetKeyBind("Move Backward",slKeyCode(SDLK_s));
	MoveLeft = slGetKeyBind("Move Left",slKeyCode(SDLK_a));
	MoveRight = slGetKeyBind("Move Right",slKeyCode(SDLK_d));
	//MoveUp = slGetKeyBind("Move Up",slKeyCode(SDLK_e));
	//MoveDown = slGetKeyBind("Move Down",slKeyCode(SDLK_q));
	Jump = slGetKeyBind("Jump",slKeyCode(SDLK_SPACE));//->onpress = OnJump;
}
