#!/bin/sh
set -eux
rm -rf built
docker build . -f builders/ubuntu/Dockerfile -t slice3d-demo-physics-ubuntu --build-arg DEBUG="-debug"
docker create --name slice3d-demo-physics-ubuntu slice3d-demo-physics-ubuntu
docker cp slice3d-demo-physics-ubuntu:/game/built built
docker rm slice3d-demo-physics-ubuntu
cd built
./slice3d-demo-physics
